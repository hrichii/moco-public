import 'package:flutter/material.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/repeat_setting/repeat_setting_page.dart';

class ConnectedRepeatSettingPage extends StatelessWidget {
  const ConnectedRepeatSettingPage({super.key, required this.alarm});
  final Alarm alarm;

  @override
  Widget build(BuildContext context) {
    return RepeatSettingPage(
      initAlarm: alarm,
    );
  }
}
