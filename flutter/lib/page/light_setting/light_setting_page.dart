import 'package:flutter/material.dart';
import 'package:moco/component/common/list_item_with_check.dart';
import 'package:moco/component/unique/light_setting/app_bar_at_light_setting.dart';
import 'package:moco/constant/radius_constant.dart';
import 'package:moco/constant/space_constant.dart';
import 'package:moco/enum/light_type.dart';
import 'package:moco/model/alarm.dart';

class LightSettingPage extends StatefulWidget {
  const LightSettingPage({super.key, required this.initialAlarm});

  final Alarm initialAlarm;

  @override
  State<LightSettingPage> createState() => _LightSettingPageState();
}

class _LightSettingPageState extends State<LightSettingPage> {
  late Alarm alarm;

  @override
  void initState() {
    alarm = widget.initialAlarm;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarAtLightSetting(
        alarm: alarm,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(SpaceConstant.sm),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(RadiusConstant.sm),
                color: Colors.white.withOpacity(0.1),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListItemWithSelect(
                    label: LightType.on.title,
                    onClick: () {
                      setState(() {
                        alarm = alarm.copyWith(lightType: LightType.on.name);
                      });
                    },
                    isSelected: alarm.lightType == LightType.on.name,
                  ),
                  ListItemWithSelect(
                    label: LightType.gradually.title,
                    onClick: () {
                      setState(() {
                        alarm =
                            alarm.copyWith(lightType: LightType.gradually.name);
                      });
                    },
                    isSelected: alarm.lightType == LightType.gradually.name,
                  ),
                  ListItemWithSelect(
                    label: LightType.off.title,
                    onClick: () {
                      setState(() {
                        alarm = alarm.copyWith(lightType: LightType.off.name);
                      });
                    },
                    isSelected: alarm.lightType == LightType.off.name,
                    useBottomDivider: false,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
