import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:moco/page/stop/connected_stop_page.dart';
import 'package:moco/router/route_path.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'stop_router.g.dart';

final rootNavigatorKey = GlobalKey<NavigatorState>();

@riverpod
class StopRouter extends _$StopRouter {
  @override
  GoRouter build() {
    return GoRouter(
      navigatorKey: rootNavigatorKey,
      debugLogDiagnostics: kDebugMode,
      initialLocation: RoutePath.stop,
      routes: <RouteBase>[
        GoRoute(
            path: RoutePath.stop,
            builder: (context, state) => const ConnectedStopPage())
      ],
    );
  }
}
