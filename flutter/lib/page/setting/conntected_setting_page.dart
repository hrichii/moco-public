import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/page/setting/setting_page.dart';

class ConnectedSettingPage extends HookConsumerWidget {
  const ConnectedSettingPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return const SettingPage();
  }
}
