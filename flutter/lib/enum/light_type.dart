enum LightType {
  on(title: "即座に明るくする"),
  gradually(title: "徐々に明るくする"),
  off(title: "なし");

  const LightType({
    required this.title,
  });

  final String title;
}
