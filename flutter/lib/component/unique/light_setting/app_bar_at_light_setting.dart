import 'package:flutter/material.dart';
import 'package:moco/component/common/base_app_bar.dart';
import 'package:moco/model/alarm.dart';

class AppBarAtLightSetting extends BaseAppBar {
  const AppBarAtLightSetting({super.key, required this.alarm});
  final Alarm alarm;

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
      leading: TextButton.icon(
        label: const Text("戻る"),
        icon: const Icon(Icons.chevron_left_outlined),
        onPressed: () {
          Navigator.of(context).pop(alarm);
        },
      ),
      title: Text("ライト",
          style: Theme.of(context)
              .textTheme
              .labelLarge!
              .copyWith(fontWeight: FontWeight.bold)),
    );
  }
}
