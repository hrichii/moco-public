import 'package:flutter/material.dart';
import 'package:moco/constant/space_constant.dart';

class LayoutUtil {
  static double _windowWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static T actionByWindowSize<T>(
      {required BuildContext context,
      required T compact,
      required T medium,
      required T expanded}) {
    if (isCompact(context)) {
      return compact;
    } else if (isMedium(context)) {
      return medium;
    } else {
      return expanded;
    }
  }

  static bool isCompact(BuildContext context) {
    if (SpaceConstant.compactThresholdWidth > _windowWidth(context)) {
      return true;
    }
    return false;
  }

  static bool isMedium(BuildContext context) {
    if (SpaceConstant.compactThresholdWidth <= _windowWidth(context) &&
        SpaceConstant.mediumThresholdWidth > _windowWidth(context)) {
      return true;
    }
    return false;
  }

  static bool isExpanded(BuildContext context) {
    if (SpaceConstant.mediumThresholdWidth < _windowWidth(context)) {
      return true;
    }
    return false;
  }
}
