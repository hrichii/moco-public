import 'dart:async';
import 'dart:io';

import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:just_audio/just_audio.dart';
import 'package:moco/color_schemes.g.dart';
import 'package:moco/constant/radius_constant.dart';
import 'package:moco/datasource/db_controller.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/router/app_router.dart';
import 'package:moco/util/notification_manager.dart';

@pragma('vm:entry-point')
void execAlarm() async {
  final now = DateTime.now();
  final nextTime = DateTime(now.year, now.month, now.day, now.hour, now.minute)
      .add(const Duration(minutes: 1));
  final nowMinute = nextTime.hour * 60 + nextTime.minute;
  //TODO 繰り返し曜日
  final result = await DbController.db.get(
    tableName: Alarm.tableName,
    where: "wakeMinute = ? and isEnable = true",
    whereArgs: [nowMinute],
  );
  if (result.isEmpty) {
    return;
  }
  Alarm targetAlarm = Alarm.fromMap(result.first);
  // Alarm targetAlarm = Alarm.init();
  if (targetAlarm.getSoundType.path == null) return;
  Duration diff = nextTime.difference(DateTime.now());
  // await Future.delayed(diff);
  final audioPlayer = AudioPlayer();
  await audioPlayer.setAsset(targetAlarm.getSoundType.path!);
  audioPlayer.play();
}

Future<void> main() async {
  final NotificationAppLaunchDetails? notificationAppLaunchDetails =
      await NotificationManager.flutterLocalNotificationsPlugin
          .getNotificationAppLaunchDetails();
  bool isStop = false;
  if (notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
    isStop = true;
  }

  // 非同期
  WidgetsFlutterBinding.ensureInitialized();
  final int backgroundAlarmId = 0;
  await AndroidAlarmManager.initialize();
  await AndroidAlarmManager.periodic(
      exact: true,
      wakeup: true,
      rescheduleOnReboot: true,
      const Duration(minutes: 1),
      backgroundAlarmId,
      execAlarm);

  // flutter_local_notifications
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.requestNotificationsPermission();

  if (Platform.isAndroid) {
    await FlutterBluePlus.turnOn();
  }
  runApp(ProviderScope(child: MainApp(isStop: isStop)));
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key, required this.isStop});

  final bool isStop;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final router =
        isStop ? ref.watch(appRouterProvider) : ref.watch(appRouterProvider);

    return MaterialApp.router(
      title: 'MOCO',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: darkColorScheme,
        appBarTheme: AppBarTheme(surfaceTintColor: Colors.transparent),
        navigationBarTheme: NavigationBarThemeData(
            shadowColor: Colors.white,
            surfaceTintColor: Colors.transparent,
            height: 60,
            indicatorColor: Colors.transparent,
            labelBehavior: NavigationDestinationLabelBehavior.alwaysShow,
            iconTheme: MaterialStateProperty.resolveWith((states) {
              if (states.contains(MaterialState.selected)) {
                return IconThemeData(
                    size: 26, color: darkColorScheme.secondary);
              } else {
                return IconThemeData(size: 26, color: Colors.grey);
              }
            }),
            labelTextStyle: MaterialStateProperty.resolveWith((states) {
              if (states.contains(MaterialState.selected)) {
                return TextStyle(
                    color: darkColorScheme.secondary, fontSize: 10);
              } else {
                return TextStyle(fontSize: 10, color: Colors.grey);
              }
            })
            // backgroundColor: darkColorScheme.secondary
            ),
        iconButtonTheme: IconButtonThemeData(
            style: ButtonStyle(
          iconColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return darkColorScheme.secondary.withOpacity(0.5);
            } else {
              return darkColorScheme.secondary;
            }
          }),
          overlayColor: MaterialStateProperty.all(Colors.transparent),
          splashFactory: NoSplash.splashFactory,
        )),
        textButtonTheme: TextButtonThemeData(
            style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return darkColorScheme.secondary.withOpacity(0.5);
            } else {
              return darkColorScheme.secondary;
            }
          }),
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          overlayColor: MaterialStateProperty.all(Colors.transparent),
          splashFactory: NoSplash.splashFactory,
        )),
        inputDecorationTheme: const InputDecorationTheme(
          border: UnderlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius:
                  BorderRadius.all(Radius.circular(RadiusConstant.sm))),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          labelStyle: TextStyle(fontSize: 12),
          filled: true,
        ),

        // pageTransitionsTheme: PageTransitionsTheme(
        //   builders: {
        //     TargetPlatform.android:,
        //     TargetPlatform.iOS:
        //         NoTransitionsBuilder(), //CupertinoPageTransitionsBuilder(),
        //     TargetPlatform.linux: NoTransitionsBuilder(),
        //     TargetPlatform.macOS: NoTransitionsBuilder(),
        //     TargetPlatform.windows: NoTransitionsBuilder(),
        //   },
        // ),
      ),
      routerConfig: router,
    );
  }
}
