abstract class Repository<T> {
  Future<List<T>> find();

  Future<int> save(T model);

  Future<int> update(T model);

  Future<int> delete(T model);
}
