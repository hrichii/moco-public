// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_alarm.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$createAlarmHash() => r'526947dbb448ff984fd3cffb965862930fb1414b';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$CreateAlarm extends BuildlessAutoDisposeAsyncNotifier<int?> {
  late final Key? key;

  FutureOr<int?> build({
    Key? key,
  });
}

/// See also [CreateAlarm].
@ProviderFor(CreateAlarm)
const createAlarmProvider = CreateAlarmFamily();

/// See also [CreateAlarm].
class CreateAlarmFamily extends Family<AsyncValue<int?>> {
  /// See also [CreateAlarm].
  const CreateAlarmFamily();

  /// See also [CreateAlarm].
  CreateAlarmProvider call({
    Key? key,
  }) {
    return CreateAlarmProvider(
      key: key,
    );
  }

  @override
  CreateAlarmProvider getProviderOverride(
    covariant CreateAlarmProvider provider,
  ) {
    return call(
      key: provider.key,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'createAlarmProvider';
}

/// See also [CreateAlarm].
class CreateAlarmProvider
    extends AutoDisposeAsyncNotifierProviderImpl<CreateAlarm, int?> {
  /// See also [CreateAlarm].
  CreateAlarmProvider({
    Key? key,
  }) : this._internal(
          () => CreateAlarm()..key = key,
          from: createAlarmProvider,
          name: r'createAlarmProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$createAlarmHash,
          dependencies: CreateAlarmFamily._dependencies,
          allTransitiveDependencies:
              CreateAlarmFamily._allTransitiveDependencies,
          key: key,
        );

  CreateAlarmProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.key,
  }) : super.internal();

  final Key? key;

  @override
  FutureOr<int?> runNotifierBuild(
    covariant CreateAlarm notifier,
  ) {
    return notifier.build(
      key: key,
    );
  }

  @override
  Override overrideWith(CreateAlarm Function() create) {
    return ProviderOverride(
      origin: this,
      override: CreateAlarmProvider._internal(
        () => create()..key = key,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        key: key,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<CreateAlarm, int?> createElement() {
    return _CreateAlarmProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CreateAlarmProvider && other.key == key;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, key.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CreateAlarmRef on AutoDisposeAsyncNotifierProviderRef<int?> {
  /// The parameter `key` of this provider.
  Key? get key;
}

class _CreateAlarmProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<CreateAlarm, int?>
    with CreateAlarmRef {
  _CreateAlarmProviderElement(super.provider);

  @override
  Key? get key => (origin as CreateAlarmProvider).key;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
