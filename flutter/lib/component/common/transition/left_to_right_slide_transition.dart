import 'package:flutter/material.dart';

class LeftToRightSlideTransition extends SlideTransition {
  LeftToRightSlideTransition({
    super.key,
    required Animation<double> animation,
    Widget? child,
  }) : super(
          position: Tween<Offset>(
            begin: const Offset(1.0, 0.0),
            end: Offset.zero,
          ).animate(
            CurvedAnimation(
              parent: animation,
              curve: Curves.easeInOutCubic,
            ),
          ),
          child: child,
        );
}
