class RadiusConstant {
  // 円弧の大きさ
  static const double xss = 2;
  static const double xs = 4;
  static const double sm = 8;
  static const double md = 12;
  static const double lg = 16;
  static const double full = 999;
}
