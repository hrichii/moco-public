import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  final StatefulNavigationShell navigationShell;
  const CustomBottomNavigationBar({super.key, required this.navigationShell});

  @override
  Widget build(BuildContext context) {
    return NavigationBar(
      selectedIndex:
          navigationShell.currentIndex < 5 ? navigationShell.currentIndex : 0,
      destinations: const [
        NavigationDestination(label: 'アラーム', icon: Icon(Icons.alarm_outlined)),
        NavigationDestination(
            label: 'ライト', icon: Icon(Icons.lightbulb_outline)),
        NavigationDestination(label: '設定', icon: Icon(Icons.settings_outlined)),
      ],
      onDestinationSelected: _onTap,
    );
  }

  void _onTap(index) {
    navigationShell.goBranch(
      index,
      initialLocation: index == navigationShell.currentIndex,
    );
  }
}
