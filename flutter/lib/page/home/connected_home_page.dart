import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/component/unique/home/app_bar_at_home.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/home/home_page.dart';
import 'package:moco/state/delete_alarm/delete_alarm.dart';
import 'package:moco/state/get_alarm/get_alarm.dart';
import 'package:moco/state/update_alarm/update_alarm.dart';

class ConnectedHomePage extends HookConsumerWidget {
  const ConnectedHomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final getAlarmProviderInstance = useMemoized(() => getAlarmProvider());
    final updateAlarmProviderInstance =
        useMemoized(() => updateAlarmProvider());
    final deleteAlarmProviderInstance =
        useMemoized(() => deleteAlarmProvider());

    AsyncValue<List<Alarm>?> asyncAlarmList =
        ref.watch(getAlarmProviderInstance);

    remove(Alarm alarm) {
      ref.read(deleteAlarmProviderInstance.notifier).request(
          alarm: alarm,
          successCallBack: () {
            ref.read(getAlarmProviderInstance.notifier).remove(id: alarm.id);
          });
    }

    update(Alarm alarm) {
      ref.read(updateAlarmProviderInstance.notifier).request(
          alarm: alarm,
          successCallBack: () {
            ref
                .read(getAlarmProviderInstance.notifier)
                .updateAlarm(newAlarm: alarm);
          });
    }

    useEffect(() {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(getAlarmProviderInstance.notifier).request();
      });
    }, const []);

    return Scaffold(
        appBar: AppBarAtHome(reload: () {
          ref.read(getAlarmProviderInstance.notifier).request();
        }),
        body: HomePage(
          asyncAlarmList: asyncAlarmList,
          remove: remove,
          update: update,
        ));
  }
}
