import 'package:flutter/material.dart';

class ColorUtil {
  static Color? primary(BuildContext context) {
    return Theme.of(context).colorScheme.primary;
  }

  static Color? onPrimary(BuildContext context) {
    return Theme.of(context).colorScheme.onPrimary;
  }

  static Color? onPrimaryContainer(BuildContext context) {
    return Theme.of(context).colorScheme.onPrimaryContainer;
  }

  static Color? primaryContainer(BuildContext context) {
    return Theme.of(context).colorScheme.primaryContainer;
  }

  static Color? inversePrimary(BuildContext context) {
    return Theme.of(context).colorScheme.inversePrimary;
  }

  static Color? secondary(BuildContext context) {
    return Theme.of(context).colorScheme.secondary;
  }

  static Color? onSecondary(BuildContext context) {
    return Theme.of(context).colorScheme.onSecondary;
  }

  static Color? error(BuildContext context) {
    return Theme.of(context).colorScheme.error;
  }

  static Color? onError(BuildContext context) {
    return Theme.of(context).colorScheme.onError;
  }

  static secondaryContainer(BuildContext context) {
    return Theme.of(context).colorScheme.secondaryContainer;
  }

  static onSecondaryContainer(BuildContext context) {
    return Theme.of(context).colorScheme.onSecondaryContainer;
  }

  static Color? background(BuildContext context) {
    return Theme.of(context).colorScheme.background;
  }

  static Color? outlineVariant(BuildContext context) {
    return Theme.of(context).colorScheme.outlineVariant;
  }
}
