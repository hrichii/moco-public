import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/sound_setting/sound_setting_page.dart';

class ConnectedSoundSettingPage extends HookConsumerWidget {
  const ConnectedSoundSettingPage({super.key, required this.alarm});
  final Alarm alarm;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SoundSettingPage(
      initialAlarm: alarm,
    );
  }
}
