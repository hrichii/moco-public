// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bluetooth.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bluetoothHash() => r'bac2de2bdb2968355839dc26a508185c229d6c1c';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$Bluetooth
    extends BuildlessAutoDisposeAsyncNotifier<BluetoothDevice?> {
  late final Key? key;

  FutureOr<BluetoothDevice?> build({
    Key? key,
  });
}

/// See also [Bluetooth].
@ProviderFor(Bluetooth)
const bluetoothProvider = BluetoothFamily();

/// See also [Bluetooth].
class BluetoothFamily extends Family<AsyncValue<BluetoothDevice?>> {
  /// See also [Bluetooth].
  const BluetoothFamily();

  /// See also [Bluetooth].
  BluetoothProvider call({
    Key? key,
  }) {
    return BluetoothProvider(
      key: key,
    );
  }

  @override
  BluetoothProvider getProviderOverride(
    covariant BluetoothProvider provider,
  ) {
    return call(
      key: provider.key,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'bluetoothProvider';
}

/// See also [Bluetooth].
class BluetoothProvider
    extends AutoDisposeAsyncNotifierProviderImpl<Bluetooth, BluetoothDevice?> {
  /// See also [Bluetooth].
  BluetoothProvider({
    Key? key,
  }) : this._internal(
          () => Bluetooth()..key = key,
          from: bluetoothProvider,
          name: r'bluetoothProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$bluetoothHash,
          dependencies: BluetoothFamily._dependencies,
          allTransitiveDependencies: BluetoothFamily._allTransitiveDependencies,
          key: key,
        );

  BluetoothProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.key,
  }) : super.internal();

  final Key? key;

  @override
  FutureOr<BluetoothDevice?> runNotifierBuild(
    covariant Bluetooth notifier,
  ) {
    return notifier.build(
      key: key,
    );
  }

  @override
  Override overrideWith(Bluetooth Function() create) {
    return ProviderOverride(
      origin: this,
      override: BluetoothProvider._internal(
        () => create()..key = key,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        key: key,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<Bluetooth, BluetoothDevice?>
      createElement() {
    return _BluetoothProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BluetoothProvider && other.key == key;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, key.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin BluetoothRef on AutoDisposeAsyncNotifierProviderRef<BluetoothDevice?> {
  /// The parameter `key` of this provider.
  Key? get key;
}

class _BluetoothProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<Bluetooth, BluetoothDevice?>
    with BluetoothRef {
  _BluetoothProviderElement(super.provider);

  @override
  Key? get key => (origin as BluetoothProvider).key;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
