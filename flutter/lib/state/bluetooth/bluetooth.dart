import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'bluetooth.g.dart';

const String DEVICE_NAME = "MOCO";
const String SERVICE_UUID = "0000fff0-0000-1000-8000-00805f9b34fb";
const String LIGHT_CHARACTERISTIC_UUID = "0000fff0-0000-1000-8000-00805f9b34fb";

@riverpod
class Bluetooth extends _$Bluetooth {
  @override
  FutureOr<BluetoothDevice?> build({Key? key}) {
    return null;
  }

  void scanAndConnect() async {
    var subscription = FlutterBluePlus.onScanResults.listen(
      (results) {
        if (results.isNotEmpty &&
            results.last.advertisementData.localName == DEVICE_NAME) {
          ScanResult r = results.last;
          FlutterBluePlus.stopScan();
          r.device.connect();
          state = AsyncData(r.device);
          // setLight(0);
        }
      },
      onError: (e) {
        print(e);
      },
    );

    await FlutterBluePlus.startScan(timeout: const Duration(seconds: 3));
  }

  Future<BluetoothService?> getLightService() async {
    return _getService(SERVICE_UUID);
  }

  Future<BluetoothCharacteristic?> getCharacteristic() async {
    return _getCharacteristic(
        serviceUuid: SERVICE_UUID,
        characteristicUuid: LIGHT_CHARACTERISTIC_UUID);
  }

  Future<BluetoothService?> _getService(String uuid) async {
    if (state.valueOrNull == null) return null;
    List<BluetoothService>? services =
        await state.valueOrNull!.discoverServices();
    for (var service in services) {
      if (service.serviceUuid == Guid(uuid)) {
        return service;
      }
    }
    print("Serviceが見つかりませんでした。");
    return null;
  }

  Future<BluetoothCharacteristic?> _getCharacteristic(
      {required String serviceUuid, required String characteristicUuid}) async {
    BluetoothService? service = await _getService(serviceUuid);
    if (service == null) {
      return null;
    }
    for (BluetoothCharacteristic c in service.characteristics) {
      if (c.characteristicUuid == Guid(characteristicUuid)) {
        return c;
      }
    }
    print("Charcteristicが見つかりませんでした。");
    return null;
  }

  Future<void> setLight(int value) async {
    BluetoothCharacteristic? c = await _getCharacteristic(
        serviceUuid: SERVICE_UUID,
        characteristicUuid: LIGHT_CHARACTERISTIC_UUID);
    await c?.write([value]);
  }
}

// final flutterBlue = await ref.read(bleProvider.future);
// final scanResult = await flutterBlue.scan().firstWhere((scanResult) {
//   return scanResult.device.name == 'YourDeviceName';
// });
// return scanResult.device;

// final characteristicProvider =
//     FutureProvider<BluetoothCharacteristic>((ref) async {
//   final device = await ref.read(deviceProvider.future);
//   final services = await device.discoverServices();
//   for (final service in services) {
//     for (final characteristic in service.characteristics) {
//       if (characteristic.properties.write) {
//         return characteristic;
//       }
//     }
//   }
//   return null;
// });
