import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:moco/component/common/scaffold_with_nav.dart';
import 'package:moco/page/home/connected_home_page.dart';
import 'package:moco/page/light/conntected_light_page.dart';
import 'package:moco/router/route_path.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'app_router.g.dart';

final rootNavigatorKey = GlobalKey<NavigatorState>();
final homeTabNavigatorKey = GlobalKey<NavigatorState>();
final lightTabNavigatorKey = GlobalKey<NavigatorState>();
final settingTabNavigatorKey = GlobalKey<NavigatorState>();

@riverpod
class AppRouter extends _$AppRouter {
  @override
  GoRouter build() {
    return GoRouter(
      navigatorKey: rootNavigatorKey,
      debugLogDiagnostics: kDebugMode,
      initialLocation: RoutePath.home,
      routes: <RouteBase>[
        StatefulShellRoute.indexedStack(
          builder: (context, state, navigationShell) =>
              ScaffoldWithNav(navigationShell: navigationShell),
          branches: [
            StatefulShellBranch(
              navigatorKey: homeTabNavigatorKey,
              routes: [
                GoRoute(
                    path: RoutePath.home,
                    builder: (context, state) => const ConnectedHomePage())
              ],
            ),
            StatefulShellBranch(
              navigatorKey: lightTabNavigatorKey,
              routes: [
                GoRoute(
                    path: RoutePath.light,
                    builder: (context, state) => const ConnectedLightPage())
              ],
            ),
            StatefulShellBranch(
              navigatorKey: settingTabNavigatorKey,
              routes: [
                GoRoute(
                    path: RoutePath.setting,
                    builder: (context, state) => Container())
              ],
            )
          ],
        )
      ],
    );
  }
}
