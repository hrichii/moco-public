import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:moco/constant/space_constant.dart';
import 'package:moco/enum/light_type.dart';
import 'package:moco/enum/repeat_type.dart';
import 'package:moco/enum/sound_type.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/util/color_util.dart';

class HomePage extends StatelessWidget {
  const HomePage(
      {super.key,
      required this.asyncAlarmList,
      required this.remove,
      required this.update});

  final AsyncValue<List<Alarm>?> asyncAlarmList;
  final Function(Alarm) remove;
  final Function(Alarm) update;

  @override
  Widget build(BuildContext context) {
    if (asyncAlarmList.isLoading) {
      return Center(
          child: CircularProgressIndicator(
        color: ColorUtil.secondary(context),
      ));
    }
    return ListView.builder(
        itemCount: asyncAlarmList.valueOrNull?.length ?? 0,
        itemBuilder: (BuildContext context, int index) {
          final Alarm alarm = asyncAlarmList.valueOrNull![index];
          return Container(
              child: Column(
            children: [
              Dismissible(
                key: Key(alarm.id.toString() ?? ""),
                direction: DismissDirection.startToEnd,
                onDismissed: (DismissDirection direction) {
                  if (direction == DismissDirection.startToEnd) {
                    remove(alarm);
                  }
                },
                background: Container(
                  color: Colors.red, //ColorUtil.error(context),
                  child: const Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.all(SpaceConstant.sm),
                        child: Icon(
                          Icons.delete_outline,
                          size: 30,
                        ),
                      )),
                ),
                child: ListTile(
                  title: Text(
                    alarm.wakeUpTimeStr,
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                  subtitle: Text(repeatText(
                      alarm.sunday,
                      alarm.monday,
                      alarm.tuesday,
                      alarm.wednesday,
                      alarm.thursday,
                      alarm.friday,
                      alarm.saturday)),
                  trailing: CupertinoSwitch(
                      value: alarm.isEnable ?? false,
                      onChanged: (value) {
                        Alarm newAlarm = alarm.copyWith(isEnable: value);
                        update(newAlarm);
                      }),
                  // subtitle: Text(
                  //     "${repeatText(alarm.sunday, alarm.monday, alarm.tuesday, alarm.wednesday, alarm.thursday, alarm.friday, alarm.saturday)}  ${lightText(alarm.lightType)}  ${soundText(alarm.soundType)}"),
                ),
              ),
            ],
          ));
        });
  }

  String repeatText(bool? sunday, bool? monday, bool? tuesday, bool? wednesday,
      bool? thursday, bool? friday, bool? saturday) {
    List<String> resultList = [];
    if (sunday ?? false) {
      resultList.add(RepeatType.sunday.title.substring(1, 2));
    }
    if (monday ?? false) {
      resultList.add(RepeatType.monday.title.substring(1, 2));
    }
    if (tuesday ?? false) {
      resultList.add(RepeatType.tuesday.title.substring(1, 2));
    }
    if (wednesday ?? false) {
      resultList.add(RepeatType.wednesday.title.substring(1, 2));
    }
    if (thursday ?? false) {
      resultList.add(RepeatType.thursday.title.substring(1, 2));
    }
    if (friday ?? false) {
      resultList.add(RepeatType.friday.title.substring(1, 2));
    }
    if (saturday ?? false) {
      resultList.add(RepeatType.saturday.title.substring(1, 2));
    }
    if (resultList.isEmpty) {
      resultList.add("しない");
    }
    return resultList.join(" ,");
  }

  String lightText(String? lightTypeStr) {
    String result = "";
    for (var lightType in LightType.values.toList()) {
      if (lightTypeStr == lightType.name) {
        result = lightType.title;
        break;
      }
    }
    if (result.isEmpty) {
      result += "なし";
    }
    return result;
  }

  String soundText(String? soundTypeStr) {
    String result = "";
    for (var soundType in SoundType.values.toList()) {
      if (soundTypeStr == soundType.name) {
        result = soundType.title;
        break;
      }
    }
    return result;
  }
}
