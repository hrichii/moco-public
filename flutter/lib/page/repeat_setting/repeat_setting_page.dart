import 'package:flutter/material.dart';
import 'package:moco/component/common/list_item_with_check.dart';
import 'package:moco/component/unique/repeat_setting/app_bar_at_repeat_setting.dart';
import 'package:moco/constant/radius_constant.dart';
import 'package:moco/constant/space_constant.dart';
import 'package:moco/enum/repeat_type.dart';
import 'package:moco/model/alarm.dart';

class RepeatSettingPage extends StatefulWidget {
  const RepeatSettingPage({
    super.key,
    required this.initAlarm,
  });
  final Alarm initAlarm;

  @override
  State<RepeatSettingPage> createState() => _RepeatSettingPageState();
}

class _RepeatSettingPageState extends State<RepeatSettingPage> {
  late Alarm alarm;
  @override
  void initState() {
    alarm = widget.initAlarm;
  }

  @override
  Widget build(BuildContext context) {
    print(alarm.monday);
    return Scaffold(
      appBar: AppBarAtRepeatSetting(
        alarm: alarm,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(SpaceConstant.sm),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(RadiusConstant.sm),
                color: Colors.white.withOpacity(0.1),
              ),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                ListItemWithSelect(
                  label: RepeatType.sunday.title,
                  onClick: () {
                    setState(() {
                      alarm = alarm.copyWith(sunday: !(alarm.sunday ?? false));
                    });
                  },
                  isSelected: alarm.sunday ?? false,
                ),
                ListItemWithSelect(
                  label: RepeatType.monday.title,
                  onClick: () {
                    setState(() {
                      alarm = alarm.copyWith(monday: !(alarm.monday ?? false));
                    });
                  },
                  isSelected: alarm.monday ?? false,
                ),
                ListItemWithSelect(
                  label: RepeatType.tuesday.title,
                  onClick: () {
                    setState(() {
                      alarm =
                          alarm.copyWith(tuesday: !(alarm.tuesday ?? false));
                    });
                  },
                  isSelected: alarm.tuesday ?? false,
                ),
                ListItemWithSelect(
                  label: RepeatType.wednesday.title,
                  onClick: () {
                    setState(() {
                      alarm = alarm.copyWith(
                          wednesday: !(alarm.wednesday ?? false));
                    });
                  },
                  isSelected: alarm.wednesday ?? false,
                ),
                ListItemWithSelect(
                  label: RepeatType.thursday.title,
                  onClick: () {
                    setState(() {
                      alarm =
                          alarm.copyWith(thursday: !(alarm.thursday ?? false));
                    });
                  },
                  isSelected: alarm.thursday ?? false,
                ),
                ListItemWithSelect(
                  label: RepeatType.friday.title,
                  onClick: () {
                    setState(() {
                      alarm = alarm.copyWith(friday: !(alarm.friday ?? false));
                    });
                  },
                  isSelected: alarm.friday ?? false,
                ),
                ListItemWithSelect(
                  label: RepeatType.saturday.title,
                  onClick: () {
                    setState(() {
                      alarm =
                          alarm.copyWith(saturday: !(alarm.saturday ?? false));
                    });
                  },
                  isSelected: alarm.saturday ?? false,
                  useBottomDivider: false,
                )
              ]),
            ),
          )
        ],
      ),
    );
  }
}
