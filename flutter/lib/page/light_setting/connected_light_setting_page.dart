import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/light_setting/light_setting_page.dart';

class ConnectedLightSettingPage extends HookConsumerWidget {
  const ConnectedLightSettingPage({super.key, required this.alarm});
  final Alarm alarm;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return LightSettingPage(
      initialAlarm: alarm,
    );
  }
}
