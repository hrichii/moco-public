import 'package:flutter/material.dart';
import 'package:moco/component/common/base_app_bar.dart';
import 'package:moco/constant/radius_constant.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/create_alarm/connected_create_alarm_page.dart';
import 'package:moco/util/notification_manager.dart';

class AppBarAtHome extends BaseAppBar {
  const AppBarAtHome({
    super.key,
    required this.reload,
  });
  final Function() reload;

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
      leading: TextButton(
          onPressed: () async {
            await Future.delayed(const Duration(seconds: 5));
            await NotificationManager.initializeNotifications();

            // Show notification
            await NotificationManager.showNotification();
          },
          child: Text(
            "編集",
          )),
      actions: [
        IconButton(
            onPressed: () async {
              final Alarm? returnedValue = await showModalBottomSheet(
                useSafeArea: true,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(
                        top: Radius.circular(RadiusConstant.md))),
                isScrollControlled: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                backgroundColor: Colors.black,
                elevation: 5,
                useRootNavigator: true,
                context: context,
                // 自前でモーダル用Navigatorを作成
                builder: (context) => Navigator(
                  onGenerateRoute: (context) =>
                      MaterialPageRoute<ConnectedCreateAlarmPage>(
                    builder: (context) => ConnectedCreateAlarmPage(),
                  ),
                ),
              );
              if (returnedValue != null) {
                reload();
              }
            },
            icon: const Icon(Icons.add_outlined))
      ],
    );
  }
}
