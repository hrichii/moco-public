class SpaceConstant {
  // Windowサイズの閾値
  static const double compactThresholdWidth = 600;
  static const double mediumThresholdWidth = 1000;

  // AppBarの高さ
  static const double appBarHeight = 60;
  static const double compactAppBarHeight = 60;

  // NavigationRailの幅
  static const double expandedNavRailWidth = 250;
  static const double mediumNavRailWidth = 56;

  static const double maxSearchBarWidth = 400;

  // 余白の大きさ
  static const double xss = 4;
  static const double xs = 8;
  static const double sm = 16;
  static const double md = 24;
  static const double lg = 32;
  static const double xl = 40;
  static const double xll = 64;
}
