import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/page/stop/stop_page.dart';

class ConnectedStopPage extends HookConsumerWidget {
  const ConnectedStopPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(appBar: AppBar(), body: StopPage());
  }
}
