import 'package:flutter/material.dart';
import 'package:moco/router/app_router.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  const BaseAppBar({super.key, this.leading, this.title, this.actions});
  final Widget? leading;
  final Widget? title;
  final List<Widget>? actions;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leadingWidth: 100,
      centerTitle: true,
      leading: Container(alignment: Alignment.centerLeft, child: leading),
      title: title,
      actions: actions,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(44);
}
