import 'package:flutter/material.dart';
import 'package:moco/component/common/base_app_bar.dart';

class AppBarAtCreateAlarm extends BaseAppBar {
  const AppBarAtCreateAlarm({super.key, required this.save});
  final Function() save;

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
      leading: TextButton(
        child: const Text("キャンセル"),
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
        },
      ),
      title: Text("アラームを追加",
          style: Theme.of(context)
              .textTheme
              .labelLarge!
              .copyWith(fontWeight: FontWeight.bold)),
      actions: [
        TextButton(
          onPressed: save,
          child: const Text("保存"),
        )
      ],
    );
  }
}
