// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_alarm.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$deleteAlarmHash() => r'bbf1216f0c1e4bc46f62ff9e79b8c42a7cc8b134';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$DeleteAlarm extends BuildlessAutoDisposeAsyncNotifier<int?> {
  late final Key? key;

  FutureOr<int?> build({
    Key? key,
  });
}

/// See also [DeleteAlarm].
@ProviderFor(DeleteAlarm)
const deleteAlarmProvider = DeleteAlarmFamily();

/// See also [DeleteAlarm].
class DeleteAlarmFamily extends Family<AsyncValue<int?>> {
  /// See also [DeleteAlarm].
  const DeleteAlarmFamily();

  /// See also [DeleteAlarm].
  DeleteAlarmProvider call({
    Key? key,
  }) {
    return DeleteAlarmProvider(
      key: key,
    );
  }

  @override
  DeleteAlarmProvider getProviderOverride(
    covariant DeleteAlarmProvider provider,
  ) {
    return call(
      key: provider.key,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'deleteAlarmProvider';
}

/// See also [DeleteAlarm].
class DeleteAlarmProvider
    extends AutoDisposeAsyncNotifierProviderImpl<DeleteAlarm, int?> {
  /// See also [DeleteAlarm].
  DeleteAlarmProvider({
    Key? key,
  }) : this._internal(
          () => DeleteAlarm()..key = key,
          from: deleteAlarmProvider,
          name: r'deleteAlarmProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$deleteAlarmHash,
          dependencies: DeleteAlarmFamily._dependencies,
          allTransitiveDependencies:
              DeleteAlarmFamily._allTransitiveDependencies,
          key: key,
        );

  DeleteAlarmProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.key,
  }) : super.internal();

  final Key? key;

  @override
  FutureOr<int?> runNotifierBuild(
    covariant DeleteAlarm notifier,
  ) {
    return notifier.build(
      key: key,
    );
  }

  @override
  Override overrideWith(DeleteAlarm Function() create) {
    return ProviderOverride(
      origin: this,
      override: DeleteAlarmProvider._internal(
        () => create()..key = key,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        key: key,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<DeleteAlarm, int?> createElement() {
    return _DeleteAlarmProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DeleteAlarmProvider && other.key == key;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, key.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin DeleteAlarmRef on AutoDisposeAsyncNotifierProviderRef<int?> {
  /// The parameter `key` of this provider.
  Key? get key;
}

class _DeleteAlarmProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<DeleteAlarm, int?>
    with DeleteAlarmRef {
  _DeleteAlarmProviderElement(super.provider);

  @override
  Key? get key => (origin as DeleteAlarmProvider).key;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
