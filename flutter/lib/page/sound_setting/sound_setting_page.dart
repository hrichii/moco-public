import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:moco/component/common/list_item_with_check.dart';
import 'package:moco/component/unique/sound_setting/app_bar_at_sound_setting.dart';
import 'package:moco/constant/radius_constant.dart';
import 'package:moco/constant/space_constant.dart';
import 'package:moco/enum/sound_type.dart';
import 'package:moco/model/alarm.dart';

class SoundSettingPage extends StatefulWidget {
  const SoundSettingPage({super.key, required this.initialAlarm});
  final Alarm initialAlarm;
  // final AudioPlayer audioPlayer;

  @override
  State<SoundSettingPage> createState() => _SoundSettingPageState();
}

class _SoundSettingPageState extends State<SoundSettingPage> {
  late Alarm alarm;
  final AudioPlayer audioPlayer = AudioPlayer();
  @override
  void initState() {
    alarm = widget.initialAlarm;
    super.initState();
  }

  @override
  void dispose() {
    audioPlayer.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarAtSoundSetting(
        alarm: alarm,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(SpaceConstant.sm),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(RadiusConstant.sm),
                color: Colors.white.withOpacity(0.1),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildListItemWithSelect(soundType: SoundType.alarm),
                  buildListItemWithSelect(soundType: SoundType.ringtone),
                  buildListItemWithSelect(soundType: SoundType.robot),
                  buildListItemWithSelect(soundType: SoundType.roosters),
                  buildListItemWithSelect(
                      soundType: SoundType.none, useBottomDivider: false),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildListItemWithSelect(
      {required SoundType soundType, bool useBottomDivider = true}) {
    return ListItemWithSelect(
      label: soundType.title,
      onClick: () async {
        audioPlayer.stop();
        setState(() {
          alarm = alarm.copyWith(soundType: soundType.name);
        });
        if (soundType.path == null) return;
        await audioPlayer.setAsset(soundType.path!);
        audioPlayer.play();
      },
      isSelected: alarm.soundType == soundType.name,
      useBottomDivider: useBottomDivider,
    );
  }
}
