enum RepeatType {
  sunday(title: "毎日曜日"),
  monday(title: "毎月曜日"),
  tuesday(title: "毎火曜日"),
  wednesday(title: "毎水曜日"),
  thursday(title: "毎木曜日"),
  friday(title: "毎金曜日"),
  saturday(title: "毎土曜日");

  const RepeatType({
    required this.title,
  });

  final String title;
}
