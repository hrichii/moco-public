enum SoundType {
  alarm(title: "アラーム", path: "assets/sound/alarm.mp3"),
  ringtone(title: "リングトーン", path: "assets/sound/ringtone.mp3"),
  robot(title: "ロボット", path: "assets/sound/robot.mp3"),
  roosters(title: "鶏の鳴き声", path: "assets/sound/roosters.mp3"),
  none(title: "なし", path: null);

  const SoundType({required this.title, required this.path});

  final String title;
  final String? path;
}
