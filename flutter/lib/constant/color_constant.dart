import 'dart:ui';

class ColorConstant {
  static const secondary0 = Color(0xFF000000);
  static const secondary10 = Color(0xFF3C0900);
  static const secondary20 = Color(0xFF611300);
  static const secondary25 = Color(0xFF731901);
  static const secondary30 = Color(0xFF881F01);
  static const secondary35 = Color(0xFF9C2703);
  static const secondary40 = Color(0xFFAD3211);
  static const secondary50 = Color(0xFFCF4B27);
  static const secondary60 = Color(0xFFF1643E);
  static const secondary70 = Color(0xFFFF8B6B);
  static const secondary80 = Color(0xFFFFB4A1);
  static const secondary90 = Color(0xFFFFDAD1);
  static const secondary95 = Color(0xFFFFEDE9);
  static const secondary98 = Color(0xFFFFF8F5);
  static const secondary99 = Color(0xFFFEFBFF);
  static const secondary100 = Color(0xFFFFFFFF);
}
