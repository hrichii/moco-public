import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:moco/constant/space_constant.dart';
import 'package:moco/util/color_util.dart';

class ListItemWithSelect extends StatelessWidget {
  const ListItemWithSelect(
      {super.key,
      required this.label,
      required this.onClick,
      required this.isSelected,
      bool? useBottomDivider})
      : useBottomDivider = useBottomDivider ?? true;
  final String label;
  final bool isSelected;
  final bool useBottomDivider;
  final Function() onClick;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onClick,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: SpaceConstant.sm),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: SpaceConstant.xs),
                child: Icon(
                  isSelected ? Icons.check : null,
                  color: ColorUtil.secondary(context),
                ),
              ),
              const Gap(SpaceConstant.sm),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          label,
                        ),
                      ],
                    ),
                  ),
                  if (useBottomDivider)
                    const Divider(
                      height: 1,
                    )
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}
