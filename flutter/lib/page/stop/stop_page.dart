import 'package:flutter/material.dart';

class StopPage extends StatelessWidget {
  const StopPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      color: Colors.red,
    );
  }
}
