import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:moco/component/common/list_item_with_push.dart';
import 'package:moco/component/common/transition/left_to_right_slide_transition.dart';
import 'package:moco/component/unique/create_alarm/app_bar_at_create_alarm.dart';
import 'package:moco/constant/radius_constant.dart';
import 'package:moco/constant/space_constant.dart';
import 'package:moco/enum/light_type.dart';
import 'package:moco/enum/repeat_type.dart';
import 'package:moco/enum/sound_type.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/light_setting/connected_light_setting_page.dart';
import 'package:moco/page/repeat_setting/connected_repeat_setting_page.dart';
import 'package:moco/page/sound_setting/connected_sound_setting_page.dart';

class CreateAlarmPage extends StatefulWidget {
  const CreateAlarmPage({super.key, required this.create});

  final Function(Alarm, Function()?) create;

  @override
  State<CreateAlarmPage> createState() => _CreateAlarmPageState();
}

class _CreateAlarmPageState extends State<CreateAlarmPage> {
  Alarm alarm = Alarm.init();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarAtCreateAlarm(
        save: () {
          widget.create(alarm, () {
            Navigator.of(context, rootNavigator: true).pop(alarm);
          });
        },
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const Gap(SpaceConstant.sm),
          SizedBox(
            height: 200,
            child: CupertinoTheme(
              data: const CupertinoThemeData(
                  textTheme: CupertinoTextThemeData(
                dateTimePickerTextStyle:
                    TextStyle(color: Colors.white, fontSize: 26),
              )),
              child: CupertinoDatePicker(
                mode: CupertinoDatePickerMode.time,
                use24hFormat: true,
                initialDateTime: alarm.wakeUpDateTime,
                onDateTimeChanged: (DateTime newDateTime) {
                  int newWakeMinute =
                      newDateTime.hour * 60 + newDateTime.minute;
                  setState(() {
                    alarm = alarm.copyWith(wakeMinute: newWakeMinute);
                  });
                },
              ),
            ),
          ),
          const Gap(SpaceConstant.sm),
          Padding(
            padding: const EdgeInsets.all(SpaceConstant.sm),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(RadiusConstant.sm),
                color: Colors.white.withOpacity(0.1),
              ),
              child: Column(
                children: [
                  ListItemWithPush(
                      label: "繰り返し",
                      value: repeatText(),
                      push: () => push(
                              page: ConnectedRepeatSettingPage(
                            alarm: alarm,
                          ))),
                  const Divider(
                    height: 1,
                  ),
                  ListItemWithPush(
                      label: "ライト",
                      value: lightText(),
                      push: () => push(
                              page: ConnectedLightSettingPage(
                            alarm: alarm,
                          ))),
                  const Divider(
                    height: 1,
                  ),
                  ListItemWithPush(
                      label: "サウンド",
                      value: soundText(),
                      push: () => push(
                              page: ConnectedSoundSettingPage(
                            alarm: alarm,
                          ))),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void push({required Widget page}) async {
    final editedAlarm = await Navigator.of(context).push(PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) =>
            page, //const ConnectedRepeatedPage(),
        transitionsBuilder: (context, animation, secondaryAnimation, child) =>
            LeftToRightSlideTransition(animation: animation, child: child)));
    setState(() {
      alarm = editedAlarm;
    });
  }

  String repeatText() {
    List<String> resultList = [];
    if (alarm.sunday ?? false) {
      resultList.add(RepeatType.sunday.title.substring(1, 2));
    }
    if (alarm.monday ?? false) {
      resultList.add(RepeatType.monday.title.substring(1, 2));
    }
    if (alarm.tuesday ?? false) {
      resultList.add(RepeatType.tuesday.title.substring(1, 2));
    }
    if (alarm.wednesday ?? false) {
      resultList.add(RepeatType.wednesday.title.substring(1, 2));
    }
    if (alarm.thursday ?? false) {
      resultList.add(RepeatType.thursday.title.substring(1, 2));
    }
    if (alarm.friday ?? false) {
      resultList.add(RepeatType.friday.title.substring(1, 2));
    }
    if (alarm.saturday ?? false) {
      resultList.add(RepeatType.saturday.title.substring(1, 2));
    }
    if (resultList.isEmpty) {
      resultList.add("しない");
    }
    return resultList.join(" ,");
  }

  String lightText() {
    String result = "";
    for (var lightType in LightType.values.toList()) {
      if (alarm.lightType == lightType.name) {
        result = lightType.title;
        break;
      }
    }
    if (result.isEmpty) {
      result += "なし";
    }
    return result;
  }

  String soundText() {
    String result = "";
    for (var soundType in SoundType.values.toList()) {
      if (alarm.soundType == soundType.name) {
        result = soundType.title;
        break;
      }
    }
    return result;
  }
}
