// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stop_router.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$stopRouterHash() => r'59d0cf4e54d81d223b0ef1b8a6f4577fb42fafcd';

/// See also [StopRouter].
@ProviderFor(StopRouter)
final stopRouterProvider =
    AutoDisposeNotifierProvider<StopRouter, GoRouter>.internal(
  StopRouter.new,
  name: r'stopRouterProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$stopRouterHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$StopRouter = AutoDisposeNotifier<GoRouter>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
