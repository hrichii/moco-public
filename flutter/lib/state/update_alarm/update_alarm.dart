import 'package:flutter/material.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/repository/alarm_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'update_alarm.g.dart';

@riverpod
class UpdateAlarm extends _$UpdateAlarm {
  @override
  FutureOr<int?> build({Key? key}) {
    return null;
  }

  void request(
      {required Alarm alarm,
      Function()? successCallBack,
      Function(Object, StackTrace)? errorCallBack}) async {
    if (state.isLoading) return;
    try {
      //ロード状態
      state = const AsyncLoading<int?>().copyWithPrevious(state);
      //APIリクエスト
      int result = await ref.read(alarmRepositoryProvider).update(alarm);
      //完了状態
      state = AsyncData(result);
      successCallBack?.call();
    } catch (error, stackTrace) {
      print("問題が発生しました。");
      state = AsyncError<int>(error, stackTrace);
      errorCallBack?.call(error, stackTrace);
    }
  }
}
