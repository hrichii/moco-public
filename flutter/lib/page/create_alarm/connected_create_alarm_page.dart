import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/page/create_alarm/create_alarm_page.dart';
import 'package:moco/state/create_alarm/create_alarm.dart';

class ConnectedCreateAlarmPage extends HookConsumerWidget {
  const ConnectedCreateAlarmPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final provider = useMemoized(() => createAlarmProvider());
    return CreateAlarmPage(
        create: (Alarm alarm, Function()? successCollBack) async {
      // final result = await ref.read(alarmRepositoryProvider).save(alarm);
      ref
          .read(provider.notifier)
          .request(alarm: alarm, successCallBack: successCollBack);
    });
  }
}
