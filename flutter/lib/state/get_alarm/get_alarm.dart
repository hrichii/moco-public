import 'package:flutter/material.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/repository/alarm_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'get_alarm.g.dart';

@riverpod
class GetAlarm extends _$GetAlarm {
  @override
  FutureOr<List<Alarm>?> build({Key? key}) {
    return null;
  }

  void request(
      {Function()? successCallBack,
      Function(Object, StackTrace)? errorCallBack}) async {
    if (state.isLoading) return;
    try {
      //ロード状態
      state = const AsyncLoading<List<Alarm>?>().copyWithPrevious(state);
      //APIリクエスト
      List<Alarm> result = await ref.read(alarmRepositoryProvider).find();
      //完了状態
      state = AsyncData(result);
      successCallBack?.call();
    } catch (error, stackTrace) {
      print("問題が発生しました。1");
      state = AsyncError<List<Alarm>?>(error, stackTrace);
      errorCallBack?.call(error, stackTrace);
    }
  }

  void remove({required int? id}) {
    List<Alarm>? alarmList = state.valueOrNull;
    alarmList?.removeWhere((alarm) => alarm.id == id);
    state = AsyncData(alarmList);
  }

  void updateAlarm({required Alarm newAlarm}) {
    List<Alarm>? alarmList = state.valueOrNull;
    final newAlarmList = alarmList?.map((alarm) {
      if (alarm.id == newAlarm.id) {
        return newAlarm;
      } else {
        return alarm;
      }
    }).toList();
    state = AsyncData(newAlarmList);
  }
}
