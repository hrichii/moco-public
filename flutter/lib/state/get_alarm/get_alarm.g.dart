// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_alarm.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$getAlarmHash() => r'c750d7440b80dfa31d1eb1ab7d9f29300aef2e35';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$GetAlarm
    extends BuildlessAutoDisposeAsyncNotifier<List<Alarm>?> {
  late final Key? key;

  FutureOr<List<Alarm>?> build({
    Key? key,
  });
}

/// See also [GetAlarm].
@ProviderFor(GetAlarm)
const getAlarmProvider = GetAlarmFamily();

/// See also [GetAlarm].
class GetAlarmFamily extends Family<AsyncValue<List<Alarm>?>> {
  /// See also [GetAlarm].
  const GetAlarmFamily();

  /// See also [GetAlarm].
  GetAlarmProvider call({
    Key? key,
  }) {
    return GetAlarmProvider(
      key: key,
    );
  }

  @override
  GetAlarmProvider getProviderOverride(
    covariant GetAlarmProvider provider,
  ) {
    return call(
      key: provider.key,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getAlarmProvider';
}

/// See also [GetAlarm].
class GetAlarmProvider
    extends AutoDisposeAsyncNotifierProviderImpl<GetAlarm, List<Alarm>?> {
  /// See also [GetAlarm].
  GetAlarmProvider({
    Key? key,
  }) : this._internal(
          () => GetAlarm()..key = key,
          from: getAlarmProvider,
          name: r'getAlarmProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getAlarmHash,
          dependencies: GetAlarmFamily._dependencies,
          allTransitiveDependencies: GetAlarmFamily._allTransitiveDependencies,
          key: key,
        );

  GetAlarmProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.key,
  }) : super.internal();

  final Key? key;

  @override
  FutureOr<List<Alarm>?> runNotifierBuild(
    covariant GetAlarm notifier,
  ) {
    return notifier.build(
      key: key,
    );
  }

  @override
  Override overrideWith(GetAlarm Function() create) {
    return ProviderOverride(
      origin: this,
      override: GetAlarmProvider._internal(
        () => create()..key = key,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        key: key,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<GetAlarm, List<Alarm>?>
      createElement() {
    return _GetAlarmProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetAlarmProvider && other.key == key;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, key.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin GetAlarmRef on AutoDisposeAsyncNotifierProviderRef<List<Alarm>?> {
  /// The parameter `key` of this provider.
  Key? get key;
}

class _GetAlarmProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<GetAlarm, List<Alarm>?>
    with GetAlarmRef {
  _GetAlarmProviderElement(super.provider);

  @override
  Key? get key => (origin as GetAlarmProvider).key;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
