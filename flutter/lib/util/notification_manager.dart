import 'dart:async';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationManager {
  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static final StreamController<String?> selectNotificationStream =
      StreamController<String?>.broadcast();

  static int counter = 0;

  static Future<void> initializeNotifications() async {
    // Android initialization settings
    var initializationSettingsAndroid =
        AndroidInitializationSettings('launch_background');

    // Initialize settings
    var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse:
          (NotificationResponse notificationResponse) {
        print(notificationResponse.notificationResponseType);
        print(notificationResponse.payload);
        switch (notificationResponse.notificationResponseType) {
          case NotificationResponseType.selectedNotification:
            selectNotificationStream.add(notificationResponse.payload);
            break;
          case NotificationResponseType.selectedNotificationAction:
            // if (notificationResponse.actionId == navigationActionId) {
            selectNotificationStream.add(notificationResponse.payload);
            // }
            break;
        }
      },
    );
  }

  static Future<void> showNotification() async {
    // Full-screen notification settings
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'full screen channel id', 'full screen channel name',
        channelDescription: 'full screen channel description',
        priority: Priority.high,
        playSound: false,
        importance: Importance.high,
        fullScreenIntent: true);

    var platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);

    // Show notification
    await flutterLocalNotificationsPlugin.show(
      counter, // Notification ID
      'Alarm Notification', // Notification title
      'Current time: ${DateTime.now().toLocal()}', // Notification body
      platformChannelSpecifics,
      payload: 'item x',
    );
    counter++;
  }
}
