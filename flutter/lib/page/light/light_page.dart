import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';

class LightPage extends StatefulWidget {
  const LightPage(
      {super.key,
      required this.setLight,
      required this.scanAndConnect,
      required this.asyncBluetoothDevice});
  final AsyncValue<BluetoothDevice?> asyncBluetoothDevice;
  final Function() scanAndConnect;
  final Function(int) setLight;

  @override
  State<LightPage> createState() => _LightPageState();
}

class _LightPageState extends State<LightPage> {
  int lightValue = 0;
  final detectorKey = GlobalKey();

  @override
  void initState() {
    widget.scanAndConnect();
  }

  @override
  Widget build(BuildContext context) {
    double controllerHeight = MediaQuery.of(context).size.height * 4.5 / 10;
    double controllerWidth = MediaQuery.of(context).size.width * 4 / 10;
    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(widget.asyncBluetoothDevice.valueOrNull.toString()),
              const Icon(
                Icons.lightbulb,
                color: Colors.white,
                size: 40,
              ),
              const Gap(64),
              GestureDetector(
                key: detectorKey,
                onPanUpdate: (DragUpdateDetails details) {
                  RenderBox renderBox = detectorKey.currentContext
                      ?.findRenderObject() as RenderBox;
                  double y = renderBox.globalToLocal(details.globalPosition).dy;

                  int newLightValue =
                      (((controllerHeight - y) / controllerHeight) * 100)
                          .clamp(0, 100)
                          .toInt();
                  widget.setLight(newLightValue);
                  setState(() {
                    lightValue = newLightValue;
                  });
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(40.0),
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    height: controllerHeight,
                    width: controllerWidth,
                    color: Colors.black.withOpacity(0.2),
                    child: Container(
                      height: controllerHeight * lightValue / 100,
                      width: controllerWidth,
                      color: Colors.white.withOpacity(0.2),
                    ),
                  ),
                ),
              ),
              const Gap(104),
            ],
          ),
        ));
  }
}
