// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_alarm.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$updateAlarmHash() => r'46538b7705deac61d363af5618f648dc679fee07';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$UpdateAlarm extends BuildlessAutoDisposeAsyncNotifier<int?> {
  late final Key? key;

  FutureOr<int?> build({
    Key? key,
  });
}

/// See also [UpdateAlarm].
@ProviderFor(UpdateAlarm)
const updateAlarmProvider = UpdateAlarmFamily();

/// See also [UpdateAlarm].
class UpdateAlarmFamily extends Family<AsyncValue<int?>> {
  /// See also [UpdateAlarm].
  const UpdateAlarmFamily();

  /// See also [UpdateAlarm].
  UpdateAlarmProvider call({
    Key? key,
  }) {
    return UpdateAlarmProvider(
      key: key,
    );
  }

  @override
  UpdateAlarmProvider getProviderOverride(
    covariant UpdateAlarmProvider provider,
  ) {
    return call(
      key: provider.key,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'updateAlarmProvider';
}

/// See also [UpdateAlarm].
class UpdateAlarmProvider
    extends AutoDisposeAsyncNotifierProviderImpl<UpdateAlarm, int?> {
  /// See also [UpdateAlarm].
  UpdateAlarmProvider({
    Key? key,
  }) : this._internal(
          () => UpdateAlarm()..key = key,
          from: updateAlarmProvider,
          name: r'updateAlarmProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$updateAlarmHash,
          dependencies: UpdateAlarmFamily._dependencies,
          allTransitiveDependencies:
              UpdateAlarmFamily._allTransitiveDependencies,
          key: key,
        );

  UpdateAlarmProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.key,
  }) : super.internal();

  final Key? key;

  @override
  FutureOr<int?> runNotifierBuild(
    covariant UpdateAlarm notifier,
  ) {
    return notifier.build(
      key: key,
    );
  }

  @override
  Override overrideWith(UpdateAlarm Function() create) {
    return ProviderOverride(
      origin: this,
      override: UpdateAlarmProvider._internal(
        () => create()..key = key,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        key: key,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<UpdateAlarm, int?> createElement() {
    return _UpdateAlarmProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UpdateAlarmProvider && other.key == key;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, key.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin UpdateAlarmRef on AutoDisposeAsyncNotifierProviderRef<int?> {
  /// The parameter `key` of this provider.
  Key? get key;
}

class _UpdateAlarmProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<UpdateAlarm, int?>
    with UpdateAlarmRef {
  _UpdateAlarmProviderElement(super.provider);

  @override
  Key? get key => (origin as UpdateAlarmProvider).key;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
