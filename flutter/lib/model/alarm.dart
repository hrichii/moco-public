import 'package:freezed_annotation/freezed_annotation.dart';
import "package:intl/intl.dart";
import 'package:moco/enum/light_type.dart';
import 'package:moco/enum/sound_type.dart';

part 'alarm.freezed.dart';
part 'alarm.g.dart';

@freezed
class Alarm with _$Alarm {
  const Alarm._();
  const factory Alarm(
      {int? id,
      int? wakeMinute,
      String? soundType,
      String? lightType,
      bool? sunday,
      bool? monday,
      bool? tuesday,
      bool? wednesday,
      bool? thursday,
      bool? friday,
      bool? saturday,
      bool? isEnable}) = _Alarm;

  factory Alarm.init() {
    return Alarm(
        wakeMinute: DateTime.now().hour * 60 + DateTime.now().minute,
        soundType: SoundType.alarm.name,
        lightType: LightType.on.name,
        sunday: false,
        monday: false,
        tuesday: false,
        wednesday: false,
        thursday: false,
        friday: false,
        saturday: false,
        isEnable: true);
  }

  factory Alarm.fromJson(Map<String, dynamic> json) => _$AlarmFromJson(json);
  //
  DateTime get wakeUpDateTime {
    final now = DateTime.now();
    final hour = (wakeMinute ?? 0) ~/ 60;
    final minute = (wakeMinute ?? 0) - (hour * 60);
    return DateTime(now.year, now.month, now.day, hour, minute);
  }

  String get wakeUpTimeStr {
    final DateTime dateTime = wakeUpDateTime;
    final String hour = dateTime.hour.toString();
    final String minute = NumberFormat("00").format(dateTime.minute);
    return "$hour:$minute";
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'wakeMinute': wakeMinute,
      'soundType': soundType,
      'lightType': lightType,
      'sunday': (sunday ?? false) ? 1 : 0,
      'monday': (monday ?? false) ? 1 : 0,
      'tuesday': (tuesday ?? false) ? 1 : 0,
      'wednesday': (wednesday ?? false) ? 1 : 0,
      'thursday': (thursday ?? false) ? 1 : 0,
      'friday': (friday ?? false) ? 1 : 0,
      'saturday': (saturday ?? false) ? 1 : 0,
      'isEnable': (isEnable ?? false) ? 1 : 0,
    };
  }

  factory Alarm.fromMap(Map<String, dynamic> map) {
    return Alarm(
      id: map['id'] as int?,
      wakeMinute: map['wakeMinute'] as int?,
      soundType: map['soundType'] as String?,
      lightType: map['lightType'] as String?,
      sunday: map['sunday'] == 1,
      monday: map['monday'] == 1,
      tuesday: map['tuesday'] == 1,
      wednesday: map['wednesday'] == 1,
      thursday: map['thursday'] == 1,
      friday: map['friday'] == 1,
      saturday: map['saturday'] == 1,
      isEnable: map['isEnable'] == 1,
    );
  }
  static String get tableName => 'alarm';
  SoundType get getSoundType {
    return SoundType.values
        .toList()
        .firstWhere((t) => t.name == soundType, orElse: () => SoundType.none);
  }

  LightType get getLightType {
    return LightType.values
        .toList()
        .firstWhere((t) => t.name == lightType, orElse: () => LightType.off);
  }
}
