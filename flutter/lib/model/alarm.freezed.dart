// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'alarm.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Alarm _$AlarmFromJson(Map<String, dynamic> json) {
  return _Alarm.fromJson(json);
}

/// @nodoc
mixin _$Alarm {
  int? get id => throw _privateConstructorUsedError;
  int? get wakeMinute => throw _privateConstructorUsedError;
  String? get soundType => throw _privateConstructorUsedError;
  String? get lightType => throw _privateConstructorUsedError;
  bool? get sunday => throw _privateConstructorUsedError;
  bool? get monday => throw _privateConstructorUsedError;
  bool? get tuesday => throw _privateConstructorUsedError;
  bool? get wednesday => throw _privateConstructorUsedError;
  bool? get thursday => throw _privateConstructorUsedError;
  bool? get friday => throw _privateConstructorUsedError;
  bool? get saturday => throw _privateConstructorUsedError;
  bool? get isEnable => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AlarmCopyWith<Alarm> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlarmCopyWith<$Res> {
  factory $AlarmCopyWith(Alarm value, $Res Function(Alarm) then) =
      _$AlarmCopyWithImpl<$Res, Alarm>;
  @useResult
  $Res call(
      {int? id,
      int? wakeMinute,
      String? soundType,
      String? lightType,
      bool? sunday,
      bool? monday,
      bool? tuesday,
      bool? wednesday,
      bool? thursday,
      bool? friday,
      bool? saturday,
      bool? isEnable});
}

/// @nodoc
class _$AlarmCopyWithImpl<$Res, $Val extends Alarm>
    implements $AlarmCopyWith<$Res> {
  _$AlarmCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? wakeMinute = freezed,
    Object? soundType = freezed,
    Object? lightType = freezed,
    Object? sunday = freezed,
    Object? monday = freezed,
    Object? tuesday = freezed,
    Object? wednesday = freezed,
    Object? thursday = freezed,
    Object? friday = freezed,
    Object? saturday = freezed,
    Object? isEnable = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      wakeMinute: freezed == wakeMinute
          ? _value.wakeMinute
          : wakeMinute // ignore: cast_nullable_to_non_nullable
              as int?,
      soundType: freezed == soundType
          ? _value.soundType
          : soundType // ignore: cast_nullable_to_non_nullable
              as String?,
      lightType: freezed == lightType
          ? _value.lightType
          : lightType // ignore: cast_nullable_to_non_nullable
              as String?,
      sunday: freezed == sunday
          ? _value.sunday
          : sunday // ignore: cast_nullable_to_non_nullable
              as bool?,
      monday: freezed == monday
          ? _value.monday
          : monday // ignore: cast_nullable_to_non_nullable
              as bool?,
      tuesday: freezed == tuesday
          ? _value.tuesday
          : tuesday // ignore: cast_nullable_to_non_nullable
              as bool?,
      wednesday: freezed == wednesday
          ? _value.wednesday
          : wednesday // ignore: cast_nullable_to_non_nullable
              as bool?,
      thursday: freezed == thursday
          ? _value.thursday
          : thursday // ignore: cast_nullable_to_non_nullable
              as bool?,
      friday: freezed == friday
          ? _value.friday
          : friday // ignore: cast_nullable_to_non_nullable
              as bool?,
      saturday: freezed == saturday
          ? _value.saturday
          : saturday // ignore: cast_nullable_to_non_nullable
              as bool?,
      isEnable: freezed == isEnable
          ? _value.isEnable
          : isEnable // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AlarmImplCopyWith<$Res> implements $AlarmCopyWith<$Res> {
  factory _$$AlarmImplCopyWith(
          _$AlarmImpl value, $Res Function(_$AlarmImpl) then) =
      __$$AlarmImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      int? wakeMinute,
      String? soundType,
      String? lightType,
      bool? sunday,
      bool? monday,
      bool? tuesday,
      bool? wednesday,
      bool? thursday,
      bool? friday,
      bool? saturday,
      bool? isEnable});
}

/// @nodoc
class __$$AlarmImplCopyWithImpl<$Res>
    extends _$AlarmCopyWithImpl<$Res, _$AlarmImpl>
    implements _$$AlarmImplCopyWith<$Res> {
  __$$AlarmImplCopyWithImpl(
      _$AlarmImpl _value, $Res Function(_$AlarmImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? wakeMinute = freezed,
    Object? soundType = freezed,
    Object? lightType = freezed,
    Object? sunday = freezed,
    Object? monday = freezed,
    Object? tuesday = freezed,
    Object? wednesday = freezed,
    Object? thursday = freezed,
    Object? friday = freezed,
    Object? saturday = freezed,
    Object? isEnable = freezed,
  }) {
    return _then(_$AlarmImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      wakeMinute: freezed == wakeMinute
          ? _value.wakeMinute
          : wakeMinute // ignore: cast_nullable_to_non_nullable
              as int?,
      soundType: freezed == soundType
          ? _value.soundType
          : soundType // ignore: cast_nullable_to_non_nullable
              as String?,
      lightType: freezed == lightType
          ? _value.lightType
          : lightType // ignore: cast_nullable_to_non_nullable
              as String?,
      sunday: freezed == sunday
          ? _value.sunday
          : sunday // ignore: cast_nullable_to_non_nullable
              as bool?,
      monday: freezed == monday
          ? _value.monday
          : monday // ignore: cast_nullable_to_non_nullable
              as bool?,
      tuesday: freezed == tuesday
          ? _value.tuesday
          : tuesday // ignore: cast_nullable_to_non_nullable
              as bool?,
      wednesday: freezed == wednesday
          ? _value.wednesday
          : wednesday // ignore: cast_nullable_to_non_nullable
              as bool?,
      thursday: freezed == thursday
          ? _value.thursday
          : thursday // ignore: cast_nullable_to_non_nullable
              as bool?,
      friday: freezed == friday
          ? _value.friday
          : friday // ignore: cast_nullable_to_non_nullable
              as bool?,
      saturday: freezed == saturday
          ? _value.saturday
          : saturday // ignore: cast_nullable_to_non_nullable
              as bool?,
      isEnable: freezed == isEnable
          ? _value.isEnable
          : isEnable // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AlarmImpl extends _Alarm {
  const _$AlarmImpl(
      {this.id,
      this.wakeMinute,
      this.soundType,
      this.lightType,
      this.sunday,
      this.monday,
      this.tuesday,
      this.wednesday,
      this.thursday,
      this.friday,
      this.saturday,
      this.isEnable})
      : super._();

  factory _$AlarmImpl.fromJson(Map<String, dynamic> json) =>
      _$$AlarmImplFromJson(json);

  @override
  final int? id;
  @override
  final int? wakeMinute;
  @override
  final String? soundType;
  @override
  final String? lightType;
  @override
  final bool? sunday;
  @override
  final bool? monday;
  @override
  final bool? tuesday;
  @override
  final bool? wednesday;
  @override
  final bool? thursday;
  @override
  final bool? friday;
  @override
  final bool? saturday;
  @override
  final bool? isEnable;

  @override
  String toString() {
    return 'Alarm(id: $id, wakeMinute: $wakeMinute, soundType: $soundType, lightType: $lightType, sunday: $sunday, monday: $monday, tuesday: $tuesday, wednesday: $wednesday, thursday: $thursday, friday: $friday, saturday: $saturday, isEnable: $isEnable)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AlarmImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.wakeMinute, wakeMinute) ||
                other.wakeMinute == wakeMinute) &&
            (identical(other.soundType, soundType) ||
                other.soundType == soundType) &&
            (identical(other.lightType, lightType) ||
                other.lightType == lightType) &&
            (identical(other.sunday, sunday) || other.sunday == sunday) &&
            (identical(other.monday, monday) || other.monday == monday) &&
            (identical(other.tuesday, tuesday) || other.tuesday == tuesday) &&
            (identical(other.wednesday, wednesday) ||
                other.wednesday == wednesday) &&
            (identical(other.thursday, thursday) ||
                other.thursday == thursday) &&
            (identical(other.friday, friday) || other.friday == friday) &&
            (identical(other.saturday, saturday) ||
                other.saturday == saturday) &&
            (identical(other.isEnable, isEnable) ||
                other.isEnable == isEnable));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      wakeMinute,
      soundType,
      lightType,
      sunday,
      monday,
      tuesday,
      wednesday,
      thursday,
      friday,
      saturday,
      isEnable);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AlarmImplCopyWith<_$AlarmImpl> get copyWith =>
      __$$AlarmImplCopyWithImpl<_$AlarmImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AlarmImplToJson(
      this,
    );
  }
}

abstract class _Alarm extends Alarm {
  const factory _Alarm(
      {final int? id,
      final int? wakeMinute,
      final String? soundType,
      final String? lightType,
      final bool? sunday,
      final bool? monday,
      final bool? tuesday,
      final bool? wednesday,
      final bool? thursday,
      final bool? friday,
      final bool? saturday,
      final bool? isEnable}) = _$AlarmImpl;
  const _Alarm._() : super._();

  factory _Alarm.fromJson(Map<String, dynamic> json) = _$AlarmImpl.fromJson;

  @override
  int? get id;
  @override
  int? get wakeMinute;
  @override
  String? get soundType;
  @override
  String? get lightType;
  @override
  bool? get sunday;
  @override
  bool? get monday;
  @override
  bool? get tuesday;
  @override
  bool? get wednesday;
  @override
  bool? get thursday;
  @override
  bool? get friday;
  @override
  bool? get saturday;
  @override
  bool? get isEnable;
  @override
  @JsonKey(ignore: true)
  _$$AlarmImplCopyWith<_$AlarmImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
