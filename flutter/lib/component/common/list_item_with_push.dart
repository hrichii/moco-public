import 'package:flutter/material.dart';
import 'package:moco/constant/space_constant.dart';

class ListItemWithPush extends StatelessWidget {
  const ListItemWithPush(
      {super.key,
      required this.label,
      required this.value,
      required this.push});
  final String label;
  final String value;
  final Function() push;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: push,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(
            horizontal: SpaceConstant.sm, vertical: SpaceConstant.xs),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Text(label),
            Expanded(child: SizedBox()),
            Text(value),
            const Icon(
              Icons.chevron_right_outlined,
              color: Colors.grey,
            )
          ],
        ),
      ),
    );
  }
}
