import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/page/light/light_page.dart';
import 'package:moco/state/bluetooth/bluetooth.dart';

class ConnectedLightPage extends HookConsumerWidget {
  const ConnectedLightPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bluetoothProviderInstance = useMemoized(() => bluetoothProvider());

    return LightPage(
      setLight: (int value) {
        ref.read(bluetoothProviderInstance.notifier).setLight(value);
      },
      scanAndConnect: () =>
          ref.read(bluetoothProviderInstance.notifier).scanAndConnect(),
      asyncBluetoothDevice: ref.watch(bluetoothProviderInstance),
    );
  }
}
