import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'costom_bottom_naviagtion_bar.dart';

class ScaffoldWithNav extends StatelessWidget {
  const ScaffoldWithNav({
    required this.navigationShell,
    super.key,
  });

  final StatefulNavigationShell navigationShell;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: navigationShell,
      bottomNavigationBar:
          CustomBottomNavigationBar(navigationShell: navigationShell),
    );
  }
}
