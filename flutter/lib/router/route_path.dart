class RoutePath {
  static const String home = '/home';
  static const String light = '/timeline';
  static const String setting = '/setting';
  static const String stop = '/stop';
}
