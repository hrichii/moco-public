import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moco/datasource/db_controller.dart';
import 'package:moco/model/alarm.dart';
import 'package:moco/repository/repository.dart';

final alarmRepositoryProvider =
    Provider<Repository<Alarm>>((ref) => AlarmRepositoryImpl());

class AlarmRepositoryImpl implements Repository<Alarm> {
  AlarmRepositoryImpl();

  @override
  Future<List<Alarm>> find({
    String? where,
    List? whereArgs,
  }) async {
    final result = await DbController.db.get(
      tableName: Alarm.tableName,
      where: where,
      whereArgs: whereArgs,
    );
    return result.map((e) => Alarm.fromMap(e)).toList();
  }

  @override
  Future<int> save(Alarm alarm) async {
    return DbController.db
        .create(tableName: Alarm.tableName, json: alarm.toMap());
  }

  @override
  Future<int> update(Alarm alarm) async {
    return await DbController.db.update(
      tableName: Alarm.tableName,
      json: alarm.toMap(),
      primaryKey: alarm.id!,
    );
  }

  @override
  Future<int> delete(Alarm alarm) async {
    return await DbController.db.delete(
      tableName: Alarm.tableName,
      primaryKey: alarm.id!,
    );
  }
}
